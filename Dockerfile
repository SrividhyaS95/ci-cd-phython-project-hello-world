# Base image
FROM python:3.8-slim-buster

# Set the working directory
WORKDIR /index

# Copy the requirements file
COPY requirements.txt .

# Install the requirements
RUN pip install -r requirements.txt

# Copy the application files
COPY . .

# Expose the port
EXPOSE 5000

# Start the application
CMD ["python", "index.py"]
